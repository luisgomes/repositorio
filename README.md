Para uso em Data Updates e Staging com Persistent Storage.
==========================================================
Gera o pod das imagens e as ferramentas de auxílio ao backup:
	compactacao e descompactacao das imagens (tar e untar para o Persistent Storage);

Criação do projeto no Openshift
1.  *Adicionar pod php no projeto openshift por meio do catágo, apontando para este repositório*
2.  *Na página Overview, selecionar o pod criado clicando no nÃº circunscrito de azul*
3.  *No canto inferior direito, clicar em ```Add Storage to POD_NAME```*

    3.1 - o ponto de montagem deve ser /opt/app-root/src/Catalog ou equivalente
4. Realizar o build e o deploy

As imagens vêm compactadas do Gitlab. É necessario rodar o untar.php para que as imagens possam ser acessadas pelos aplicativos.

    a) Chamando a URL da aplicação no Openshift adicionando /untar.php no final: <url.openshiftapps.com>/untar.php
    
    ou
    
    b) Rodando um job 
    
        Clicar no ícone do usuário no console do Opensshift
        
        Copiar o Login Command
        
        Abrir shell no computador local que tenha o comando oc instalado
        
        Colar o Login Command para ter acesso ao projeto
        
        executar
        
            oc run phpuntar --image=php --restart='Never' --command -- /usr/bin/curl <url.openshiftapps.com>/untar.php
            
            oc get pods *(para verificar o término do job)*
            
            oc delete pod phpuntar
            
            


	
