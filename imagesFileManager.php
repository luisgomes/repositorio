<?php
/*
	API imagesFileManagerV1.php

	This script provides a RESTful API interface for a web application

	Input:

		$_GET['format'] = [ json | html | xml ]
		$_GET['method'] = [ createSubDir | deleteImage ]

	Output: A formatted HTTP response

*/

// --- Step 1: Initialize variables and functions

/**
 * Deliver HTTP Response
 * @param string $format The desired HTTP response content type: [json, html, xml]
 * @param string $api_response The desired HTTP response data
 * @return void
 **/
function deliver_response($format, $api_response){

	// Define HTTP responses
	$http_response_code = array(
		200 => 'OK',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		403 => 'Forbidden',
		404 => 'Not Found'
	);

	// Set HTTP Response
	header('HTTP/1.1 '.$api_response['status'].' '.$http_response_code[ $api_response['status'] ]);

	// Process different content types
	if( strcasecmp($format,'json') == 0 ){

		// Set HTTP Response Content Type
		header('Content-Type: application/json; charset=utf-8');

		// Format data into a JSON response
		$json_response = json_encode($api_response);

		// Deliver formatted data
		echo $json_response;

	}elseif( strcasecmp($format,'xml') == 0 ){

		// Set HTTP Response Content Type
		header('Content-Type: application/xml; charset=utf-8');

		// Format data into an XML response (This is only good at handling string data, not arrays)
		$xml_response = '<?xml version="1.0" encoding="UTF-8"?>'."\n".
			'<response>'."\n".
			"\t".'<code>'.$api_response['code'].'</code>'."\n".
			"\t".'<data>'.$api_response['data'].'</data>'."\n".
			'</response>';

		// Deliver formatted data
		echo $xml_response;

	}else{

		// Set HTTP Response Content Type (This is only good at handling string data, not arrays)
		header('Content-Type: text/html; charset=utf-8');

		// Deliver formatted data
		echo $api_response['data'];

	}

	// End script process
	exit;

}

// Define whether an HTTPS connection is required
$HTTPS_required = FALSE;

// Define whether user authentication is required
$authentication_required =  FALSE;  //  TRUE;

// Define API response codes and their related HTTP response
$api_response_code = array(
	0 => array('HTTP Response' => 400, 'Message' => 'Unknown Error'),
	1 => array('HTTP Response' => 200, 'Message' => 'Success'),
	2 => array('HTTP Response' => 403, 'Message' => 'HTTPS Required'),
	3 => array('HTTP Response' => 401, 'Message' => 'Authentication Required'),
	4 => array('HTTP Response' => 401, 'Message' => 'Authentication Failed'),
	5 => array('HTTP Response' => 404, 'Message' => 'Invalid Request'),
	6 => array('HTTP Response' => 400, 'Message' => 'Invalid Response Format')
);

// Set default HTTP response of 'ok'
$response['code'] = 0;
$response['status'] = 404;
$response['data'] = NULL;

// --- Step 2: Authorization

// Optionally require connections to be made via HTTPS
if( $HTTPS_required && $_SERVER['HTTPS'] != 'on' ){
	$response['code'] = 2;
	$response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
	$response['data'] = $api_response_code[ $response['code'] ]['Message'];

	// Return Response to browser. This will exit the script.
	deliver_response($_GET['format'], $response);
}

// Optionally require user authentication
if( $authentication_required ){

	if( empty($_POST['username']) || empty($_POST['password']) ){
		$response['code'] = 3;
		$response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
		$response['data'] = $api_response_code[ $response['code'] ]['Message'];

		// Return Response to browser
		deliver_response($_GET['format'], $response);

	}

	// Return an error response if user fails authentication. This is a very simplistic example
	// that should be modified for security in a production environment
	elseif( $_POST['username'] != 'admin' && $_POST['password'] != 'openshift' ){
		$response['code'] = 4;
		$response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
		$response['data'] = $api_response_code[ $response['code'] ]['Message'];

		// Return Response to browser
		deliver_response($_GET['format'], $response);

	}

}

/**
 * Returns the path of a successfully created subdir Line underneath a Publisher's images directory.
 * @param $pub_name The Publisher's name
 * @param $line_name The Line to be created as a subdir of the given Pubisher's path
 * @return  path for the given Line of a Publisher that was created/exists
 **/
function createSubDirFromPublisher($pub_name, $line_name){

    $dirPath = "Catalog/assets/". $pub_name;
 
    if(file_exists($dirPath) && is_dir($dirPath)){
        $subDir = "$dirPath" ."/". str_replace(' ','',$line_name);
        if(!file_exists($subDir) && !is_dir($subDir)) { 
            mkdir($subDir, 0777, true);   
        }    
        $response = $subDir . " exists or was created.";
    } else {       
        echo "error";
        $response = " oh-oh";
    }
    return $response;
}

/**
 * Returns true if successfully deleted an image file AND the subdir Line under a Publisher's images directory.
 * @param $pub_name The Publisher's name
 * @param $line_name The Line's name 
 * @param $image_name The image's name to be deleted
 * @return 'true' if image and subdir were deleted, 'false' if only the image was deleted and 'null' (= does nothing) if file/s didn't exist.
 **/
function deleteImageFromPublishersLine($pub_name, $line_name, $image_name){
    
    $subDir = str_replace(' ','',$line_name);
    $dirPath = "Catalog/assets/". "$pub_name" ."/". "$subDir" ;
    $filePath = "$dirPath" ."/". "$image_name";
    // echo $filePath;
    if(file_exists($filePath) && is_dir($dirPath)){
        unlink($filePath);  // Delete the file 
        $empty = true;
        $objects = scandir($dirPath);
        foreach ($objects as $object) { 
           if ($object != "." && $object != "..") {
             $empty = false;
           }
        }
        if ($empty) rmdir($dirPath);  // If empty, delete the dir
    }
    
    return $response = $empty;
}

/**
 * Returns true if successfully renamed the given image file to image_newName and false otherwise
 * @param $pub_name The Publisher's name
 * @param $line_name The Line's name 
 * @param $image_name The image's name to be renamed with extension
 * @param $image_newName The new image's filename with it's extension
 * @return 'true' if image was renamed, 'false' if not.
 **/
function renameFileFromPublishersLine($pub_name, $line_name, $image_name, $image_newName){
    
    $dirPath = "Catalog/assets/". $pub_name;
    $response = false;
    if(file_exists($dirPath) && is_dir($dirPath)){
        $subDirPath = "$dirPath" ."/". str_replace(' ','',$line_name);      
        if(file_exists($subDirPath) && is_dir($subDirPath)) {
            $filePathOld = "$subDirPath" ."/". "$image_name";
            $filePathNew = "$subDirPath" ."/". "$image_newName";
            $response = rename($filePathOld, $filePathNew);  
        }    
    } else {       
        echo "error";
        $response = " oh-gosh";
    }
    return $response;   
}

/**
 * Returns filePath if successfully compied an image accessable by the url and saved to subdir Line under a Publisher's images directory.
 * @param $pub_name The Publisher's name
 * @param $line_name The Line's name (can be a blank space)
 * @param $image_name The image's URLto be copied
 * @return  filePath 
 **/
function saveImageToPublishersLine($pub_name, $line_name, $image_url){    // img_name ?
    $url = $image_url;	
		//remove any query string and get the file name
		if ($image_url = parse_url($image_url)) {
			$cleanUrl = $image_url['scheme'].$image_url['host'].$image_url['path'];
		}           
    //get the pathinfo() of the url
    $cleanUrl = pathinfo($cleanUrl);
    // echo $cleanUrl; echo " ";
    //get the file name
    $image_name = $cleanUrl['basename'];
   // echo $image_name; echo " "; 
    $subDir = str_replace(' ','',$line_name);   
  //  if ($subdir == strtok($image-name, '_')) {
        if (isset($subDir) && $subDir === '') {  
            $filePath = "Catalog/assets/". "$pub_name" ."/". "$image_name" ;
        } else {
                $filePath = "Catalog/assets/". "$pub_name" ."/". "$subDir" ."/". "$image_name" ;
        }
        // echo $filePath;
        file_put_contents(
            $filePath,
            file_get_contents( $url )
        );
        $response = $filePath . " copied successfully";
        
  //  } else {
  //      $response = "Image's name " . $image_name . " prefix doesn't match the Line name " . $line_name ;
  //  }
    
 // You must have allow_url_fopen enabled in your php.ini   
 // file_put_contents($filePath, fopen($url,'r'));
 // if  copy('http://example.com/image.php', 'local/folder/flower.jpg') return true;
 
    return $response;
}


// --- Step 3: Process Request

  $method = $_GET['method'];          
 	// build payload //
    $response['code'] = 1;
    $response['api_version'] = '1.0.3';
    $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];


 switch($method) {
    
        case 'createSubDir':
            if ( $_GET['pub_name'] && $_GET['line_name'] ){
              
               $response['data'] =  createSubDirFromPublisher($_GET['pub_name'], $_GET['line_name']);
            }
            break;    
                
        case 'deleteImage':
            if ( $_GET['pub_name'] && $_GET['line_name'] && $_GET['image_name'] ) {
                
               $response['data'] = deleteImageFromPublishersLine($_GET['pub_name'], $_GET['line_name'], $_GET['image_name']);
            }
            break;
        
         case 'saveImage':
            if ( $_GET['pub_name'] && $_GET['line_name'] && $_GET['image_url'] ) {
                
               $response['data'] = saveImageToPublishersLine($_GET['pub_name'], $_GET['line_name'], $_GET['image_url']);
            }
            break;
        case 'renameImage':
            if ( $_GET['pub_name'] && $_GET['line_name'] && $_GET['image_name'] && $_GET['image_newName'] ) {
            
                $response['data'] = renameFileFromPublishersLine($_GET['pub_name'], $_GET['line_name'], $_GET['image_name'], $_GET['image_newName']);
            }
            break;     
        default:
	    echo "Invalid Method";
            break;
 }
 
 
// Method A: Say Hello to the API
if( strcasecmp($_GET['method'],'hello') == 0){
	$response['code'] = 1;
	$response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
	$response['data'] = ' Hello World';
}

// --- Step 4: Deliver Response

// Return Response to browser
deliver_response($_GET['format'], $response);

 
// ** $authentication_required = TRUE :
// curl -d 'username=admin&password=openshift'-X http://localhost:8888/imagesFileManager.php?method=hello&format=json
// curl -d 'username=admin&password=openshift'-X 'http://localhost:8888/imagesFileManager.php?method=createSubDir&pub_name=Life&line_name=Hammer Strength&format=json'
// curl -d 'username=admin&password=openshift' 'http://localhost:8888/imagesFileManager.php?method=deleteImage&pub_name=Life&line_name=HammerStrength&image_name=EagleNX_LegPress.png&format=json'
// ** = FALSE :
// http://localhost:8888/imagesFileManager.php?method=hello&format=json
// http://localhost:8888/imagesFileManager.php?method=createSubDir&pub_name=Matrix&line_name=Ultra Series&format=json
// http://localhost:8888/imagesFileManager.php?method=deleteImage&pub_name=Matrix&line_name=Varsity Series&image_name=equipo.jpg&format=json

// imagesFileManager.php?method=saveImage&pub_name=Life&line_name=Optima Series&image_url=http://php7-inicial.193b.starter-ca-central-1.openshiftapps.com/Catalog/assets/Life/Insignia/INSIGNIA_AssistDipChin.jpg&format=json
// http://localhost:8888/imagesFileManager.php?method=saveImage&pub_name=Life&line_name=Optima%20Series&image_url=http://php7-inicial.193b.starter-ca-central-1.openshiftapps.com/Catalog/assets/Life/Insignia/INSIGNIA_AssistDipChin.jpg&format=json
// imagesFileManager.php?method=renameImage&pub_name=Technogym&line_name=Artis&image_name=Artis_ChestPress.jpg&image_newName=Artis_ChestPress(Artis).jpg&format=json

?>
